package com.example.fms.controller;

import com.example.fms.exception.FileStorageException;
import com.example.fms.model.DBFile;
import com.example.fms.payload.DeleteFileResponse;
import com.example.fms.payload.UploadFileResponse;
import com.example.fms.payload.UploadFileResponseAsByteArray;
import com.example.fms.service.DBFileStorageService;
import com.example.fms.service.FileStorageService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private DBFileStorageService dbFileStorageService;
    
    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
    	
    	// Save file to storage
    	String storedFileName = fileStorageService.storeFile(file);
    	
    	if (!StringUtils.hasText(storedFileName)) {
			throw new FileStorageException("Could not store file " + file.getName() + ". Please try again!");
		}
    	
    	// Save file info to database
        DBFile dbFile = dbFileStorageService.storeFile(file);

        return new UploadFileResponse(dbFile.getName(), file.getContentType(), file.getSize());
    }
    
    @PostMapping("/updateFile/{fileId}")
    public UploadFileResponse updateFile(@RequestParam("file") MultipartFile file, @PathVariable String fileId) {

    	// Save file to storage
    	String storedFileName = fileStorageService.storeFile(file);
    	
    	if (!StringUtils.hasText(storedFileName)) {
    		throw new FileStorageException("Could not store file " + file.getName() + ". Please try again!");
    	}
    	
    	// Update file info
    	DBFile dbFile = dbFileStorageService.updateFile(file, fileId);
    	
    	return new UploadFileResponse(dbFile.getName(), file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{fileId}")
    public UploadFileResponseAsByteArray downloadFile(@PathVariable String fileId, HttpServletRequest request) {
    	
        // Load file from database
        DBFile dbFile = dbFileStorageService.getFile(fileId);

        byte[] fileData = fileStorageService.loadFileAsByteArray(dbFile.getName());
        
        return new UploadFileResponseAsByteArray(dbFile.getName(), dbFile.getType(), dbFile.getSize(), fileData);
    }
    
    @GetMapping("/deleteFile/{fileId}")
    public DeleteFileResponse deleteFile(@PathVariable String fileId, HttpServletRequest request) {
    	
    	// Load file from database
        DBFile dbFile = dbFileStorageService.getFile(fileId);

    	// Delete file from database
    	boolean isDeleted = dbFileStorageService.deleteFile(fileId);
    	
    	// Delete file from Storage
    	fileStorageService.deleteFile(dbFile.getName());
    	
    	String status = "";
    	if (isDeleted) {
    		status = "Successful";
    		return new DeleteFileResponse(dbFile.getName(), dbFile.getType(), dbFile.getSize(), status);
		} else {
			status = "Fault occured while deleting file!";
			return new DeleteFileResponse(dbFile.getName(), dbFile.getType(), dbFile.getSize(), status);
		}
    	
    }
    
    @GetMapping("/getAllFiles/")
    public List<DBFile> getAllFiles() {
    	
    	// Get all files from database
    	List<DBFile> dbFileList = dbFileStorageService.getAllFiles();
    	
    	return dbFileList;
    }
    
    @GetMapping("/getFilesByName/{fileName}")
    public List<DBFile> getFilesByName(@PathVariable String fileName) {
    	
    	// Search file from database
    	List<DBFile> dbFileList = dbFileStorageService.searchFilesByName(fileName);
    	
    	return dbFileList;
    }
}