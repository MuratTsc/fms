package com.example.fms.repository;

import com.example.fms.model.DBFile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, String> {

	@Query("SELECT f FROM DBFile f WHERE f.name LIKE :fileName")
	List<DBFile> findFilesByName(@Param("fileName") String fileName);
	
}