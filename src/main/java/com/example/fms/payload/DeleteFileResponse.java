package com.example.fms.payload;

public class DeleteFileResponse extends UploadFileResponse {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DeleteFileResponse(String fileName, String fileType, long size, String status) {
		super(fileName, fileType, size);
		this.status = status;
	}

}
