package com.example.fms.payload;

public class UploadFileResponseAsByteArray extends UploadFileResponse {

	private byte[] fileData;

	public UploadFileResponseAsByteArray(String fileName, String fileType, long size,
			byte[] fileData) {
		super(fileName, fileType, size);
		this.fileData = fileData;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

}