package com.example.fms.service;

import com.example.fms.exception.FileStorageException;
import com.example.fms.exception.FileNotFoundException;
import com.example.fms.model.DBFile;
import com.example.fms.property.FileStorageProperties;
import com.example.fms.repository.DBFileRepository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
//import java.io.IOException;

@Service
public class DBFileStorageService {

	@Autowired
	private DBFileRepository dbFileRepository;

	private FileStorageProperties fsProperties;

	public DBFile storeFile(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		String extension = fileName.substring(fileName.lastIndexOf("."));
		String path = "./uploads/" + fileName;
		String type = file.getContentType();
		long size = file.getSize();
		Date uploadDate = new Date();

		DBFile dbFile = new DBFile(fileName, extension, path, type, size, uploadDate);

		return dbFileRepository.save(dbFile);
	}
	
	public DBFile updateFile(MultipartFile file, String fileId) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		String extension = fileName.substring(fileName.lastIndexOf("."));
		String path = "./uploads/" + fileName;
		String type = file.getContentType();
		long size = file.getSize();
		Date uploadDate = new Date();
		
		DBFile dbFile = new DBFile(fileName, extension, path, type, size, uploadDate);
		dbFile.setId(fileId);
		return dbFileRepository.save(dbFile);
	}
	
	public DBFile getFile(String fileId) {
		return dbFileRepository.findById(fileId)
				.orElseThrow(() -> new FileNotFoundException("File not found with id " + fileId));
	}
	
	public List<DBFile> searchFilesByName(String fileName) {
		if(!StringUtils.hasText(fileName) || fileName.length() < 3) {
			throw new FileNotFoundException("fileName is null or length is less then 3 character");
		}
		return dbFileRepository.findFilesByName("%"+fileName+"%");
	}
	
	public boolean deleteFile(String fileId) {
		try {
			dbFileRepository.deleteById(fileId);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public List<DBFile> getAllFiles() {
		return dbFileRepository.findAll();
	}
}